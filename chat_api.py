import json  # parse the json object
import urllib  # for accessing the url
import re  # to parse the regular expressions
from bs4 import BeautifulSoup  # to get the title string from the url
from collections import defaultdict  # to return the dictionary object


def parse_chat_message(message):
    """
    This function parses a chat message to
    identify mentions, Emoticons and Links
    and returns as a json object
    Args: message(str)
    Returns: json metadata(str)
    Raises: TypeError if message is not a string
    """
    # Handle the case where there is type error in the message
    if type(message) != str:
        raise TypeError("{0} is not valid, "
                        "Please input a string".format(message))

    # using defaultdict to have a iteratable list inside the dictionary
    build_dict = defaultdict(list)

    regex = {
        # mentions - '@\w+' using Unicode (str) pattern start with '@' and
        # ends when hitting a non-word character
        "mentions": r"@\w+",
        # emoticons - '\(\w+?\)'emoticons which are alphanumeric strings,
        # no longer than 15 characters, contained in parenthesis.
        "emoticons": r"\(\w+?\)",
        # links - 'https?://[^\s]+' URLs contained in the message,
        # along with the page's title
        # TO DO should we try to match other url formats as well?
        "links": r"https?://[^\s]+"

    }

    # extracting the regular expression patterns
    regex_pattern = r"{0}|{1}|{2}".format(regex["mentions"],
                                          regex["emoticons"],
                                          regex["links"])

    # using re.findall to return all non-overlapping matches
    # of pattern in string, as a list of strings.
    # The string is scanned left-to-right,
    # and matches are returned in the order found.
    for m in re.findall(regex_pattern, message):
        # conditions to find the appropriate matches
        if re.match(regex["mentions"], m):
            key = "mentions"
            # using slice operator to get just the mentions without the '@'
            value = m[1:]
        elif re.match(regex["emoticons"], m):
            key = "emoticons"
            # using slice operator to get
            # just the emoticons without the '()'
            value = m[1:-1]
        elif re.match(regex["links"], m):
            url = m
            # Get the html data from the URL
            f = urllib.urlopen(url)
            html = f.read()
            # Get the title string
            title = BeautifulSoup(html, "html.parser").title.string
            key = "links"
            value = {
                "url": url,
                "title": title
            }
        # appends processed keys and values to the the defaultdict
        build_dict[key].append(value)

    # returns a json dump of the built dictionary
    return json.dumps(build_dict)