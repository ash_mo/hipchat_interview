import json  # to parse json
import unittest  # to test units of code
from chat_api import parse_chat_message  # to get the chat parser function


class TestChatApi(unittest.TestCase):
    """ Test class that tests the result
        output of the parse_chat_message function
    """
    def test_mentions(self):
        """ Tests mentions """
        message = "@chris and @ana you guys around?"
        result = parse_chat_message(message)
        expected = json.dumps({
            "mentions": ["chris", "ana"]
        })
        self.assertEqual(result, expected)

    def test_emoticons(self):
        """ Tests emoticons """
        message = "Good morning! (megusta)(coffee)"
        result = parse_chat_message(message)
        expected = json.dumps({
            "emoticons": ["megusta", "coffee"]
        })
        self.assertEqual(result, expected)

    def test_links(self):
        """ Tests url links """
        message = "Olympics are starting soon; http://www.nbcolympics.com"
        result = parse_chat_message(message)
        expected = json.dumps({
            "links": [
                {
                    "url": "http://www.nbcolympics.com",
                    "title": "2018 PyeongChang Olympic Games | NBC Olympics"
                }
            ]
        })
        self.assertEqual(result, expected)

    def test_all_methods(self):
        """ Tests mentions, emoticons and url"""
        message = "@bob @john (success) such a cool feature; " \
                  "https://twitter.com/jdorfman/status/430511497475670016"
        result = parse_chat_message(message)
        expected = json.dumps({
            "mentions": [
                "bob",
                "john"
            ],
            "emoticons": [
                "success"
            ],
            "links": [
                {
                    "url":
                    "https://twitter.com/jdorfman/status/430511497475670016",
                    "title":
                    "Justin Dorfman; on Twitter: \"nice @littlebigdetail from "
                    "@HipChat (shows hex colors when pasted in chat). "
                    "http://t.co/7cI6Gjy5pq\""
                }
            ]
        })
        self.assertEqual(result, expected)

    def test_empty_message(self):
        """ Tests if there is a empty message"""
        message = ""
        result = parse_chat_message(message)
        expected = '{}'
        self.assertEqual(result, expected)

    def test_type_error(self):
        """ Tests TypeError if it occurs"""
        with self.assertRaises(TypeError):
            message = 1234
            result = parse_chat_message(message)
            expected = "1234 is not valid, Please input a string"
            self.assertEqual(result, expected)
