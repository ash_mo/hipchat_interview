# Chat Api Parser #

A simple app that takes chat message input as a string parses it into an appropriate json object.


### Setup ###

Set up a virtual environment and run the source code.

For more info: https://virtualenv.pypa.io/en/stable/

* $ cd hipchat_interview

* $ virtualenv env

* $ source env/bin/activate

* $ python chat_api.py

### Dependencies ###

To get all the dependencies for running tests

* $ pip install -r requirements.txt

### Test ###

* $ pytest test_chat_api.py